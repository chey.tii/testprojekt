﻿using PriceAlertObjects;
using System;
using System.Collections.Generic;

namespace CarPriceAlertObjects
{
    public class Car
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public List<CarPrice> Prices { get; set; }

        public Car()
        {
            Prices = new List<CarPrice>();
        }

    }
}