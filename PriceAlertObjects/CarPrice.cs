﻿using System;

namespace PriceAlertObjects
{
    public class CarPrice
    {
        public DateTime Day { get; set; }
        public Decimal Price { get; set; }
    }
}