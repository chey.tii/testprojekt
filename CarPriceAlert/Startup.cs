﻿using System;
using CarPriceAlert.Models;
using CarPriceAlertObjects;
using Microsoft.Owin;
using Owin;
using PriceAlertObjects;

[assembly: OwinStartupAttribute(typeof(CarPriceAlert.Startup))]
namespace CarPriceAlert
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            LoadCarPriceDB();
            LoadCarDB();
            ConfigureAuth(app);
            //Leberwurst
        }

        private void LoadCarDB()
        {
            Storage.carDB.Add(1, CreateCar("Audi S3", "schnelles Auto", 50000, 48900));
            Storage.carDB.Add(20, CreateCar("Audi S4", "schnelles Auto", 60000, 55900));
            Storage.carDB.Add(33, CreateCar("Audi S5", "schnelles Auto", 80000, 75900));
            Storage.carDB.Add(54, CreateCar("Audi S8", "schnelles Auto", 90000, 85900));
        }

        private void LoadCarPriceDB()
        {
            Storage.carPriceDB.Add(new PriceAlert() { FirstName = "Matthias", LastName = "Weber", EMail = "matthias@makecodegreatagain.org", Link = "www.zauberhaft.de" });
            Storage.carPriceDB.Add(new PriceAlert() { FirstName = "Fritz", LastName = "Kola", EMail = "fritz@makecolagreatagain.org", Link = "www.tollecola.de" });
        }

        private Car CreateCar(string Name, string Description, decimal Price1, decimal Price2)
        {
            Car car = new Car();

            car.Description = Description;
            car.Name = Name;
            car.Prices.Add(new CarPrice { Day = DateTime.Now.AddDays(-1), Price = Price1 });
            car.Prices.Add(new CarPrice { Day = DateTime.Now, Price = Price2 });

            return car;
        }
    }
}


