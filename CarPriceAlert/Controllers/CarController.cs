﻿using CarPriceAlert.Models;
using PriceAlertObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CarPriceAlert.Controllers
{
    public class CarController : Controller
    {
       
        // GET: Car
        public ActionResult Index()
        {
            List<DTOCar> carToShow = new List<DTOCar>();

            foreach (var car in Storage.carDB)
                carToShow.Add(new DTOCar(car.Value, car.Key));

            return View(carToShow);
        }

        // GET: Car/Details/5
        public ActionResult Details(int id)
        {



            if (Storage.carDB.Where(c => c.Key.Equals(id)).Any())
            {
                return View("~/Views/Car/ASingleCar.cshtml", new DTOCar(Storage.carDB[id], id));
            }
            else
            {

                return View("~/Views/Car/NoCar.cshtml");
            }


        }

        public ActionResult ExtendedDetails(int id)
        {



            if (Storage.carDB.Where(c => c.Key.Equals(id)).Any())
            {
                return View("~/Views/Car/ASingleCarExtended.cshtml", new DTOCar(Storage.carDB[id], id));
            }
            else
            {

                return View("~/Views/Car/NoCar.cshtml");
            }


        }

        public ActionResult ExtendendDetails()
        {
            List<DTOCar> carToShow = new List<DTOCar>();

            foreach (var car in Storage.carDB)
                carToShow.Add(new DTOCar(car.Value, car.Key));

            return View(carToShow);
        }



        // POST: Car/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Car/Edit/5
        public ActionResult Edit(int id)
        {
            //https://docs.microsoft.com/de-de/dotnet/api/system.linq.enumerable.any?view=netframework-4.7.2
            if (!Storage.carDB.Where(c => c.Key.Equals(id)).Any())
                return View("~/Views/Car/NotAvailableCar.cshtml");

            return View(new DTOCar(Storage.carDB[id]));
        }

        // POST: Car/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Car/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Car/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
