﻿using CarPriceAlert.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CarPriceAlert.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class PriceAlertController : Controller
    {
         // GET: PriceAlert
        public ActionResult Index()
        {
            return View(Storage.carPriceDB);
        }

        // GET: PriceAlert/Details/5
        public ActionResult Details(int id)
        {
            return View(Storage.carPriceDB[id]);
        }

        // GET: PriceAlert/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PriceAlert/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: PriceAlert/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: PriceAlert/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: PriceAlert/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: PriceAlert/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
