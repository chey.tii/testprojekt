﻿using CarPriceAlertObjects;
using PriceAlertObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarPriceAlert.Models
{
    public class DTOCar
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price1 { get; set; }
        public decimal Price2 { get; set; }
        public decimal Price3 { get; set; }
        public DateTime Date1 { get; set; }
        public DateTime Date2 { get; set; }
        public DateTime Date3 { get; set; }
        public int Id { get; set; }

        public DTOCar(Car Car, int Id)
        {
            CreateDTOCar(Car);
            this.Id = Id;
        }

        public DTOCar(Car Car)
        {
            CreateDTOCar(Car);

        }

        private void CreateDTOCar(Car Car)
        {
            this.Description = Car.Description;
            this.Name = Car.Name;

            this.Price1 = Car.Prices[0].Price;
            this.Price2 = Car.Prices[1].Price;

            this.Date1 = Car.Prices[0].Day;
            this.Date2 = Car.Prices[1].Day;

        }

    }
}