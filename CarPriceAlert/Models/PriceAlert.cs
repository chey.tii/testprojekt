﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarPriceAlert.Models
{
    public class PriceAlert
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EMail { get; set; }
        public string Link { get; set; }
    }
}